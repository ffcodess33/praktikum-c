/*
   Nama Program : Akar1.c
   Tgl buat     : 18 Oktober 2023
   Deskripsi    : menentukan jenis akar persamaan kuadrat
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  system("clear");

  float a = 0.0, b = 0.0, c = 0.0, D = 0.0;

  printf("Menentukan Jenis Akar Persamaan Kuadrat\n");
  printf(" Persamaan Umum : ax ^ 2 + bx + c \n\n");

  printf("a = ");scanf("%f", &a);
  printf("b = ");scanf("%f", &b);
  printf("c = ");scanf("%f", &c);
  printf("\n");

  D = (b * b) - (4 * a * c);
  
  printf("Nilai Diskriminan : %5.2d\n", D);
  printf("Jenis Akar : \n");
  
  if (D == 0)
  {
    printf("Kembar\n");
  }else{
    if (D > 0)
    {
      printf("Berlainan\n");
    }else{
      if (D < 0)
      {
        printf("imajiner berlainan\n"); 
      }
    }
  }

  return 0;
}