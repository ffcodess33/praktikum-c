/*
   Nama Program : upah.c
   Tgl buat     : 18 Oktober 2023
   Deskripsi    : membuat menu
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main()
{
  system("clear");
  
  const float pi = 3.14;
  int pilih = 0;
  float sisi = 0.0, jari = 0.0, tinggi = 0.0;

  printf("        <<< Menu >>>        \n\n");
  
  printf("1. Menghitung Isi Kubus\n");
  printf("2. Menghitung Luas Lingkaran\n");
  printf("3. Menghitung Isi Silisnde\n\n");
  
  printf("Pilih Nomor : ");scanf("%d",&pilih);

  switch (pilih)
  {
  case 1:
    printf("Panjang Sisi Kubus : ");scanf("%f",&sisi);
    printf("Isi Kubus : %10.2f\n",sisi*sisi*sisi);
    break;
  case 2:
    printf("Jari-jari lingkaran : ");scanf("%f",&jari);
    printf("Luas Lingkaran : %10.2f\n",pi*jari*jari);
    break;
  case 3:
    printf("Jari-jari lingkaran : ");scanf("%f",&jari);
    printf("Tinggi Silinder : ");scanf("%f",&tinggi);
    printf("Isi Silinder : %10.2f\n",pi*jari*jari*tinggi);
    break;
  }

  return 0;
}